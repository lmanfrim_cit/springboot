package com.ciandt.training.dao;

import com.ciandt.training.model.Product;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;


public interface ProductRepository extends JpaRepository<Product, Long> {

    List<Product> findByActive(boolean active);
}
