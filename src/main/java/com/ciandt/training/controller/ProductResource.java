package com.ciandt.training.controller;

import com.ciandt.training.dao.ProductRepository;
import com.ciandt.training.model.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/produtos")
public class ProductResource {

    @Autowired
    private ProductRepository productRepository;// <<< Repositório de produtos.

    @GetMapping("/{id}")
    public Product buscar(@PathVariable Long id) {
        return productRepository.getOne(id);
    }

    @GetMapping("/active/{active}")
    public List<Product> buscar(@PathVariable Boolean active) {
        return productRepository.findByActive(active);
    }

    @GetMapping
    public List<Product> pesquisar() {
        return productRepository.findAll();
    }

    @PostMapping
    public Product salvar(@RequestBody Product produto) {
        return productRepository.save(produto);
    }

    @DeleteMapping("/{id}")
    public void deletar(@PathVariable Long id) {
        productRepository.deleteById(id);
    }

    /*@GetMapping
    public List<Product> pesquisar(
            @RequestParam(defaultValue = "nome") String ordenacao,
            @RequestParam(defaultValue = "ASC") Sort.Direction direcao) {
        return productRepository.findAll(new Sort(direcao, ordenacao));
    }

    @GetMapping
    public Page<Product> pesquisarPagina(
            @RequestParam(defaultValue = "0") int pagina,
            @RequestParam(defaultValue = "10") int porPagina,
            @RequestParam(defaultValue = "nome") String ordenacao,
            @RequestParam(defaultValue = "ASC") Sort.Direction direcao) {
        return productRepository.findAll(PageRequest.of(pagina, porPagina, new Sort(direcao, ordenacao)));
    }*/
}
